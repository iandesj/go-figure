# go-figure
===========

##Just an attempt to write my own configuration utilities for machines or whatever. All in Go. This should be fun.

####Simple setup:
```SHELL
$ mkdir ~/workspace/go && cd ~/workspace/go
$ git clone https://github.com/iandesj/go-figure.git
$ cd go-figure
$ source .gopath
$ sh goprep.sh
$ which figure # make sure all compiled correctly
$ figure
```
whatever is in the packages file should be installed - only works on debian right now. currently I've only got two parameters to install package; the package name, and the package version. these two params must be exact.
