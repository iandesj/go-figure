package main

import (
	p "figure/packaging"
)

func main() {
	filepath := "./json/packages.json"
	pkgjson := p.PreparePackages(filepath)
	for k := range pkgjson {
		p.PackageInstall(pkgjson[k].Name, pkgjson[k].Version)
	}
}
