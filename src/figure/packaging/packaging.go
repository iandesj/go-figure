package packaging

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

var distros = map[string]string{
	"ubuntu": "debian",
	"debian": "debian",
	"redhat": "redhat",
	"centos": "redhat",
	"fedora": "redhat",
	"arch":   "redhat",
	"mac os": "apple",
}

type JsonObject struct {
	Packages []PackagesType `json:"packages"`
}

type PackagesType struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

// This function will take in the path to packages json file
// and prepare it in proper json format structure
func PreparePackages(jsonpath string) []PackagesType {
	file, err := ioutil.ReadFile(jsonpath)
	if err != nil {
		fmt.Printf("File error: %v\n", err)
		os.Exit(1)
	}

	var pkgs JsonObject
	json.Unmarshal(file, &pkgs)
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}
	return pkgs.Packages
}

// This function will take in the name and version of
// packages and install them via apt
func PackageInstall(name string, version string) {
	var ostype = CheckOSType()
	fmt.Println(ostype)
	var pkg string
	if version == "default" {
		pkg = name
	} else {
		s := []string{name, version}
		switch ostype {
		case "debian":
			pkg = strings.Join(s, "=")
		case "redhat":
			pkg = strings.Join(s, "-")
		case "apple":
			pkg = strings.Join(s, " ")
		default:
			pkg = "undefined"
		}
	}
	var pkginstaller string
	switch ostype {
	case "debian":
		pkginstaller = "apt-get"
	case "redhat":
		pkginstaller = "yum"
	case "apple":
		pkginstaller = "brew"
	}
	var cmd []byte
	var err error
	if ostype == "apple" {
		cmd, err = exec.Command(pkginstaller, "install", pkg, "-y").Output()
	} else {
		cmd, err = exec.Command("sudo", pkginstaller, "install", pkg, "-y").Output()
	}
	if err != nil {
		log.Println(err.Error(), "Package", pkg, "not installed or modified")
	} else {
		log.Println("package", pkg, "installed, modified, or is already installed")
	}
	// TODO: move printing out of the function
	fmt.Println(string(cmd))
}

func CheckOSType() string {
	var ostype string

	if linux_result, lux := CheckLinux(); linux_result {
		ostype = lux
	} else if mac_result, mac := CheckMac(); mac_result {
		ostype = mac
	} else {
		ostype = ""
	}
	return ostype
}

func CheckLinux() (bool, string) {
	var release string
	var ostype string
	file, err := ioutil.ReadFile("/etc/os-release")
	if err != nil {
		log.Println(err.Error(), "Checking for non-linux machine")
	}
	release = string(file)
	// ostype is initially release information
	ostype = strings.ToLower(release)
	contains := false
	for contains == false {
		for key := range distros {
			// ostype is searched to contain a substring of
			// various distros
			if strings.Contains(ostype, key) {
				ostype = distros[key]
				contains = true
				break
			}
		}
		break
	}
	return contains, ostype
}

func CheckMac() (bool, string) {
	sw_vers, err := exec.Command("sw_vers").Output()
	if err != nil {
		log.Println(err.Error(), "Machine is not a Macintosh")
	}
	release := strings.ToLower(string(sw_vers))
	if strings.Contains(release, "mac os") {
		return true, "apple"
	} else {
		return false, "undefined"
	}
}
