package packaging

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

var os_type string = CheckOSType()

func TestPreparePackages(t *testing.T) {
	jsonpath := "packages_test.json"
	json := PreparePackages(jsonpath)

	assert.NotNil(t, json)
	assert.Equal(t, reflect.Slice, reflect.TypeOf(json).Kind(),
		"returned json structure should be a slice data structure")
}

func TestCheckingForOSType(t *testing.T) {

	switch os_type {
	case "debian", "redhat":
		present, system := CheckLinux()
		assert.Equal(t, true, present,
			"function should return true if linux is the operating system present")
		assert.Equal(t, os_type, system,
			"function should return debian if linux os is that type")
	case "apple":
		present, system := CheckMac()
		assert.Equal(t, true, present,
			"function should return true if apple is the operating system present")
		assert.Equal(t, os_type, system,
			"function should return apple if os is that type")
	}
}
