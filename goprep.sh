#! /bin/bash

# install testify for testing
go get github.com/stretchr/testify

# prepare packages - compile and test
cd src/figure/packaging
go install
go test -v
cd -
go install figure
