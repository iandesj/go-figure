#TODO

####Configuration
I'd like to add configuration utilities that can be used to quickly install a slew of packages, configure things how I like and want, and maybe even remotely perform these activies via some kind of server. But that will take more work and more security than I actually know right now. There is a lot of software that can do this already (Chef, Ansible, many more) but I think it would be fun to learn Go by building something difficult, and building my own (miniature) tool for such a similar purpose.

####In Progress
* Package management and installation [working]
* Unpacking archives from a remote store [not working]
 * Manipulating, installing, doing stuff with it's contents [not working]
* Process, application, and services configuration [not working]
* Directory structure and/or creation [not working]

####Backlog
* Provisioning of remote machines via some http server
* "Plugin" system for tacking on more utilities
* Concurrency?
* Shell script generation after successful utility run (just cuz that would be neat-o)


##* I am working on handling all exceptions and errors properly, this is a learning exercise first and foremost.
